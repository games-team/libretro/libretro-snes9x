libretro-snes9x (1.63+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Jonathan McDowell <noodles@earth.li>  Sat, 25 Jan 2025 17:47:56 +0000

libretro-snes9x (1.61+dfsg-1) unstable; urgency=medium

  * New upstream release
    * Switch to snes9x.com upstream
    * Remove patches applied upstream
    * Update watch file
    * Update upstream metadata pointers
    * Update debian/copyright
  * Switch to debhelper-compat, level 13
    * Drop executable install file
  * Add Vcs-* fields pointing to Salsa
  * Remove Sérgio Benjamim from uploaders, since he seems to be no longer
    active, and add myself.
  * Add debian/gbp.conf pointing to main as the Debian branch
  * Bump Standards-Version to 4.6.2
  * Set Rules-Requires-Root to no
  * Mark package as XS-Autobuild: yes

 -- Jonathan McDowell <noodles@earth.li>  Sun, 15 Jan 2023 16:41:34 +0000

libretro-snes9x (1.53+git20160522-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with GCC 11, thanks to Adrian Bunk.  (Closes: #998228)
  * Add upstream metadata.
  * Add watch file.

 -- Andreas Beckmann <anbe@debian.org>  Tue, 22 Nov 2022 15:17:39 +0100

libretro-snes9x (1.53+git20160522-1) unstable; urgency=low

  * Initial release (Closes: #820643).

 -- Sérgio Benjamim <sergio_br2@yahoo.com.br>  Sun, 12 Jun 2016 20:56:00 -0300
